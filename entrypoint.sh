#!/bin/bash

password_temp_file=/tmp/admin_password

color_reset="\033[0;38m";
color_red="\033[0;91m";
color_green="\033[0;92m";
color_yellow="\033[0;93m";
color_blue="\033[0;94m";
color_purple="\033[0;95m";
color_cyan="\033[0;96m";
color_white="\033[0;97m";

if [ -z "${SECRET_KEY}" ] ; then
    echo -e "${color_red}Error${color_reset}: ${color_cyan}SECRET_KEY${color_reset} env param cannot be empty"
    exit 2
fi

if [ -z "$(cat ${SETTINGS_PATH}/${SETTINGS_FILE})" ] ; then
    cp ${APP_PATH}/movingbox/settings/settings.py ${SETTINGS_PATH}/${SETTINGS_FILE}
fi

cp ${SETTINGS_PATH}/${SETTINGS_FILE} ${APP_PATH}/movingbox/settings/settings.py


INIT_FLAG=/srv/initialized

if ${ALREADY_INIT} ; then
    python ${APP_PATH}/manage.py collectstatic --clear --no-input && \
      echo -e "${color_blue}INFO${color_reset}: Parameter ${color_cyan}ALREADY_INIT${color_reset} is set to '${color_white}${ALREADY_INIT}${color_reset}'. Don't need to initialize database." && \
      touch ${INIT_FLAG}
fi

if [ ! -f ${INIT_FLAG} ] ; then
    ADMIN_PASSWORD="${ADMIN_PASSWORD}"
    if [ -z "${ADMIN_PASSWORD}" ] ; then
        ADMIN_PASSWORD="$(rand-keygen -s 12)"
        echo "${ADMIN_PASSWORD}" > ${password_temp_file}
        echo -e "${color_yellow}Warning${color_reset}: using generated password. It can be found under '${color_white}${password_temp_file}${color_reset}' file."
    fi
    ADMIN_USERNAME="${ADMIN_USERNAME:-admin}"
    ADMIN_EMAIL="${ADMIN_EMAIL:-changeit@example.com}"
    python ${APP_PATH}/manage.py collectstatic --clear --no-input
    [ $? -eq 0 ] && python ${APP_PATH}/manage.py migrate
    [ $? -eq 0 ] && python ${APP_PATH}/manage.py createdefaultgroups
    [ $? -eq 0 ] && python ${APP_PATH}/manage.py createadminuser --no-input --email "${ADMIN_EMAIL}" --username "${ADMIN_USERNAME}" --password "${ADMIN_PASSWORD}"
    ret_code=$?
    if [ ${ret_code} -eq 0 ] ; then
        echo -e "${color_blue}INFO${color_reset}: ${color_green}Successfully${color_reset} initialized application"
        touch ${INIT_FLAG}
    else
        echo -e "${color_red}Error${color_reset}: An error occurred while initializing application"
        exit ${ret_code}
    fi
fi

echo -e "${color_blue}INFO${color_reset}: ${color_green}Stating${color_reset} the server..."

supervisord &
pid="$!"
trap "echo -en '\n${color_blue}INFO${color_reset}: ${color_red}Stopping${color_reset} server...\n'; kill -SIGTERM ${pid}" SIGINT SIGTERM

while kill -0 ${pid} > /dev/null 2>&1; do
    wait
done

exit 0
