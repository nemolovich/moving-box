FROM python:3.7.4
LABEL maintainer="Brian GOHIER<nemolovich@hotmail.fr>"

ENV APP_PATH="/src/moving-box"
ENV SETTINGS_PATH="/src/settings/"
ENV SETTINGS_FILE="settings.py"
ENV APP_PORT=8080

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y nginx supervisor \
  && rm -rf /var/lib/apt/lists/*

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen
RUN while [ ! -x /usr/bin/rand-keygen ] ; do chmod +x /usr/bin/rand-keygen ; sleep 1 ; done && rand-keygen && exit $?

ENV SECRET_KEY=""
ENV ALREADY_INIT=false

VOLUME ${SETTINGS_PATH}

EXPOSE ${APP_PORT}

STOPSIGNAL SIGTERM

WORKDIR ${APP_PATH}

COPY . ${APP_PATH}

RUN touch "${SETTINGS_PATH}/${SETTINGS_FILE}" \
  && mv ${APP_PATH}/conf/nginx.conf /etc/nginx/sites-available/default \
  && mv ${APP_PATH}/conf/supervisord.conf /etc/supervisor/conf.d/default.conf \
  && rmdir ${APP_PATH}/conf \
  && mv ${APP_PATH}/entrypoint.sh /root/entrypoint.sh

RUN pip install --no-cache-dir -r requirements.txt

ENTRYPOINT [ "/root/entrypoint.sh" ]
