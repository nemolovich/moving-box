import importlib
from difflib import SequenceMatcher
from typing import Dict, Any, List, Optional
from urllib.parse import urlparse, urlencode

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm, AdminPasswordChangeForm
from django.core.paginator import Paginator, Page, EmptyPage, PageNotAnInteger
from django.forms import Form
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.http import urlencode
from unidecode import unidecode

from movingbox.models import AppUser


def get_settings(module_name: str) -> Dict[str, str]:
    module: Any = importlib.import_module(module_name)
    all_keys: List[str] = [key for key in dir(module) if key.isupper()]
    return {key.lower(): getattr(module, key) for key in all_keys}


def get_app_settings() -> Dict[str, str]:
    return get_settings('movingbox.settings')


def get_app_setting(key: str, default: str = None) -> Optional[str]:
    app_settings: Dict[str, str] = get_app_settings()
    if key in app_settings:
        return app_settings[key]
    else:
        return default


APP_NAME: str = get_app_setting('app_name')


def build_url(url: str, query_kwargs: Optional[Dict[str, str]] = None):
    if query_kwargs is None:
        query_kwargs = {}
    if len(query_kwargs) < 1:
        return url
    else:
        return u'%s?%s' % (url, urlencode(query_kwargs))


def get_paginator_page(paginator: Paginator, page_number: int) -> Page:
    paginator_page: Page
    try:
        paginator_page = paginator.page(page_number)
    except PageNotAnInteger:
        paginator_page = paginator.page(1)
    except EmptyPage:
        paginator_page = paginator.page(paginator.num_pages)
    return paginator_page


def update_per_page_session_cookie(request: HttpRequest, per_page_attr: str, cookie_attr: str) -> int:
    per_page: int = request.GET.get(per_page_attr)
    if per_page is None:
        per_page = request.session.get(cookie_attr, settings.DEFAULT_PER_PAGE)
    if per_page is None:
        per_page = settings.DEFAULT_PER_PAGE
    per_page = max(int(per_page), min(settings.PER_PAGE_VALUES))
    request.session[cookie_attr] = per_page
    return per_page


def reverse_lazy_query(view_name: str, kwargs: Optional[Dict[str, str]] = None,
                       query_kwargs: Optional[Dict[str, str]] = None) -> str:
    url = reverse_lazy(view_name, kwargs=kwargs)
    if query_kwargs:
        return build_url(url, query_kwargs)
    return url


def __get__title__(title: str = '') -> str:
    return ('%s | %s' if len(title) else '%s%s') % (title, APP_NAME)


def __matches__(compare: str, text: str, case_sensitive: bool = True) -> float:
    text1: str = compare if case_sensitive else compare.lower()
    text2: str = text if case_sensitive else text.lower()
    ratio: float = max([SequenceMatcher(None, unidecode(text1), unidecode(word)).ratio() for word in text2.split()])
    return len(text1) > 2 and text1 in text2 or ratio > 0.75


def get_index_url() -> str:
    return reverse_lazy('movingbox:index')


def get_reverse_url(url: str, args: List[Any] = None) -> str:
    if args is None:
        args = []
    return reverse_lazy(url, args=args)


def get_login_url() -> str:
    return reverse_lazy('movingbox:login')


def get_restricted_url() -> str:
    return reverse_lazy('movingbox:restricted')


def get_previous_url(request: HttpRequest, default: str = get_index_url()):
    return urlparse(request.META['HTTP_REFERER']).path if 'HTTP_REFERER' in request.META else default


def update_password(request: HttpRequest, pk: int, account_edit: bool = False) -> HttpResponse:
    user: AppUser = AppUser.objects.get(id=pk)
    is_user_edition: bool = not account_edit
    if account_edit and request.user.username != user.username:
        messages.error(request, 'Incorrect form.')
        return render(request, 'user/password.html', locals())
    else:
        title: str = __get__title__('Update password')
        password_form: Form
        if request.method == 'POST':
            password_form = PasswordChangeForm(user, request.POST)
            if account_edit:
                password_form = PasswordChangeForm(user, request.POST)
            else:
                password_form = AdminPasswordChangeForm(user, request.POST)
            if password_form.is_valid():
                user = password_form.save()
                if account_edit:
                    update_session_auth_hash(request, user)
                    messages.success(request, 'Your password was successfully updated!')
                    return HttpResponseRedirect(reverse_lazy('movingbox:account', args=[pk]))
                else:
                    messages.success(request, 'Password successfully updated!')
                    return HttpResponseRedirect(reverse_lazy('movingbox:get_user', args=[pk]))
            else:
                messages.error(request, 'Please correct the error below.')
        else:
            if account_edit:
                password_form = PasswordChangeForm(user)
            else:
                password_form = AdminPasswordChangeForm(user)
        return render(request, 'user/password.html', locals())
