from functools import wraps
from typing import Callable
from urllib.parse import urlparse

from django.conf import settings
from django.shortcuts import resolve_url

from movingbox.models import AppUser


def user_passes_test(test_func: Callable[[AppUser], bool], login_url: str = None, restricted_url: str = None):
    """
    Decorator for views that checks that the user passes the given test,
    redirecting to the log-in page if necessary. The test should be a callable
    that takes the user object and returns True if the user passes.
    """

    def decorator(view_func: Callable):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request.user):
                return view_func(request, *args, **kwargs)
            path = request.build_absolute_uri()
            if request.user.is_authenticated:
                resolved_login_url = resolve_url(restricted_url or settings.RESTRICTED_URL)
            else:
                resolved_login_url = resolve_url(login_url or settings.LOGIN_URL)
            # If the login url is the same scheme and net location then just
            # use the path as the "next" url.
            login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
            current_scheme, current_netloc = urlparse(path)[:2]
            if ((not login_scheme or login_scheme == current_scheme) and
                    (not login_netloc or login_netloc == current_netloc)):
                path = request.get_full_path()
            from django.contrib.auth.views import redirect_to_login
            return redirect_to_login(path, resolved_login_url)

        return _wrapped_view

    return decorator


def mover_required(function: Callable[[AppUser], bool] = None, login_url: str = None, restricted_url: str = None):
    """
    Decorator for views that checks that the user is mover in, redirecting
    to the log-in page if necessary.
    """
    actual_decorator = user_passes_test(
        lambda u: u.is_mover(),
        login_url=login_url,
        restricted_url=restricted_url
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


def resident_required(function: Callable[[AppUser], bool] = None, login_url: str = None, restricted_url: str = None):
    """
    Decorator for views that checks that the user is resident in, redirecting
    to the log-in page if necessary.
    """
    actual_decorator = user_passes_test(
        lambda u: u.is_resident(),
        login_url=login_url,
        restricted_url=restricted_url
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


def admin_required(function: Callable[[AppUser], bool] = None, login_url: str = None, restricted_url: str = None):
    """
    Decorator for views that checks that the user is admin in, redirecting
    to the log-in page if necessary.
    """
    actual_decorator = user_passes_test(
        lambda u: u.is_admin(),
        login_url=login_url,
        restricted_url=restricted_url
    )
    if function:
        return actual_decorator(function)
    return actual_decorator
