from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import PermissionDenied

from movingbox.views.utils import get_restricted_url


class MoverRequiredMixin(LoginRequiredMixin):
    """Verify that the current user is mover."""
    restricted_url: str = get_restricted_url()

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or not request.user.is_mover():
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def get_restricted_url(self) -> str:
        return self.restricted_url or self.get_login_url()

    def handle_no_permission(self):
        if self.raise_exception:
            raise PermissionDenied(self.get_permission_denied_message())
        redirect_url: str
        if self.request.user.is_authenticated:
            redirect_url = self.get_restricted_url()
        else:
            redirect_url = self.get_login_url()
        return redirect_to_login(self.request.get_full_path(), redirect_url, self.get_redirect_field_name())


class ResidentRequiredMixin(MoverRequiredMixin):
    """Verify that the current user is resident."""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or not request.user.is_resident():
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class AdminRequiredMixin(ResidentRequiredMixin):
    """Verify that the current user is admin."""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or not request.user.is_admin():
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
