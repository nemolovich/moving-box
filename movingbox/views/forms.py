from typing import Dict

from django.contrib.auth.forms import UserCreationForm, UsernameField, UserChangeForm
from django.contrib.auth.models import Group
from django.db.models import Model
from django.forms import Field

from movingbox.models import AppUser


class AppUserChangeForm(UserChangeForm):
    class Meta:
        model: Model = AppUser
        fields: Field = ['username', 'first_name', 'last_name', 'email']
        field_classes: Dict[str, Field] = {'username': UsernameField}


class AppUserCreationForm(UserCreationForm):
    class Meta:
        model: Model = AppUser
        fields: Field = ["username"]
        field_classes: Dict[str, Field] = {'username': UsernameField}

    def save(self, commit=True):
        user: AppUser = super(AppUserCreationForm, self).save(commit=True)
        user.groups.add(Group.objects.get(name='Mover'))
        if commit:
            user.save()
        return user
