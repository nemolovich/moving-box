from typing import List, Any, Dict

from django.db.models import Model
from django.forms import Form
from django.http import HttpResponse, HttpRequest
from django.urls import reverse_lazy
from django.views.decorators.http import require_http_methods

from movingbox.models import AppUser
from movingbox.views.decorators import resident_required
from movingbox.views.forms import AppUserCreationForm, AppUserChangeForm
from movingbox.views.generic_views import ElementView, ElementsListView, AddElementView, UpdateElementView, \
    DeleteElementView
from movingbox.views.mixins import ResidentRequiredMixin
from movingbox.views.utils import get_login_url, get_restricted_url, update_password, get_previous_url


class UserView(ResidentRequiredMixin, ElementView):
    model: Model = AppUser
    template_name: str = 'user/user.html'

    def get_view_title(self):
        return 'User [%d]' % self.object.id

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'user': self.object,
        })
        return context


class UsersView(ElementsListView):
    model: Model = AppUser
    template_name: str = 'user/users.html'
    context_object_name: str = 'users'
    view_title: str = 'Users list'


class AddUserView(AddElementView):
    model: Model = AppUser
    element_name: str = 'User'
    fields: List[str] = None
    redirect_url: str = 'movingbox:get_user'
    cancel_url: str = 'movingbox:get_users'
    form_title: str = 'Add an User'
    submit_button: str = 'Create User'
    form_class: Form = AppUserCreationForm

    def get_redirect_args(self) -> List[Any]:
        return [self.object.id]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'user': self.object,
            'account_form': context['form'],
        })
        return context


class UpdateUserView(UpdateElementView):
    model: Model = AppUser
    fields: List[str] = None
    element_name: str = 'User'
    redirect_url: str = 'movingbox:get_user'
    cancel_url: str = 'movingbox:get_user'
    form_title: str = 'Update an User'
    submit_button: str = 'Save User'
    form_class: Form = AppUserChangeForm

    def get_extra_button(self):
        return {
            'text': 'Update Password',
            'icon': 'lock',
            'color': 'info',
            'url': reverse_lazy('movingbox:password_user', args=[self.object.id]),
        }

    def get_cancel_args(self) -> List[Any]:
        return [self.object.id]

    def get_redirect_args(self) -> List[Any]:
        return [self.object.id]

    def get_previous_url(self) -> str:
        previous_url: str = get_previous_url(self.request, self.get_cancel_url())

        if previous_url == reverse_lazy('movingbox:password_user', args=[self.object.id]):
            previous_url = self.get_cancel_url()

        return previous_url


class DeleteUserView(DeleteElementView):
    model: Model = AppUser
    element_name: str = 'User'
    redirect_url: str = 'movingbox:get_users'
    cancel_url: str = 'movingbox:get_user'
    form_title: str = 'Remove an User'
    submit_button: str = 'Delete User'

    def get_cancel_args(self) -> List[Any]:
        return [self.object.id]


@resident_required(login_url=get_login_url(), restricted_url=get_restricted_url())
@require_http_methods(["GET", "POST"])
def update_user_password(request: HttpRequest, pk: int) -> HttpResponse:
    return update_password(request, pk)
