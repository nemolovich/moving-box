from typing import List, Any, Dict

from django.db.models import Model
from django.db.models.query import QuerySet

from movingbox.models import Furniture
from movingbox.views.generic_views import AddElementView, UpdateElementView, DeleteElementView, ElementView, \
    ElementsListView


class FurnitureView(ElementView):
    model: Model = Furniture
    template_name: str = 'furniture/furniture.html'

    def get_view_title(self) -> str:
        return 'Furniture[%d]' % self.object.id


class FurniturePrintView(FurnitureView):
    template_name: str = 'printer/furniture.html'


class FurnituresView(ElementsListView):
    model: Model = Furniture
    template_name: str = 'furniture/furnitures.html'
    context_object_name: str = 'furnitures'
    view_title: str = 'Furnitures list'
    queryset: QuerySet = Furniture.objects.order_by('name')
    cookie_per_page: str = 'furnitures_per_page'
    is_paginated: bool = True

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context.update({
            'display_room': True,
            'display_header': True,
        })
        return context


class AddFurnitureView(AddElementView):
    model: Model = Furniture
    fields: List[str] = ['name', 'room']
    element_name: str = 'Furniture'
    redirect_url: str = 'movingbox:get_furniture'
    cancel_url: str = 'movingbox:get_furnitures'
    form_title: str = 'Add a Furniture'
    submit_button: str = 'Create Furniture'

    def get_redirect_args(self) -> List[Any]:
        return [self.object.id]


class UpdateFurnitureView(UpdateElementView):
    model: Model = Furniture
    fields: List[str] = ['name', 'room']
    element_name: str = 'Furniture'
    redirect_url: str = 'movingbox:get_furniture'
    cancel_url: str = 'movingbox:get_furniture'
    form_title: str = 'Update a Furniture'
    submit_button: str = 'Save Furniture'

    def get_cancel_args(self) -> List[Any]:
        return [self.object.id]

    def get_redirect_args(self) -> List[Any]:
        return [self.object.id]


class DeleteFurnitureView(DeleteElementView):
    model: Model = Furniture
    element_name: str = 'Furniture'
    redirect_url: str = 'movingbox:get_furnitures'
    cancel_url: str = 'movingbox:get_furniture'
    form_title: str = 'Remove a Furniture'
    submit_button: str = 'Delete Furniture'

    def get_cancel_args(self) -> List[Any]:
        return [self.object.id]
