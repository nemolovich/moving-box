from typing import List, Any

from django.conf import settings
from django.contrib import messages
from django.db.models import Model
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.http import require_GET, require_POST
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger, Page

from movingbox.models import Item, Furniture
from movingbox.templatetags.filters import sort_items, sort_items_furnitures, sort_furnitures
from movingbox.views.decorators import resident_required
from movingbox.views.generic_views import AddElementView, UpdateElementView, DeleteElementView, ElementView
from movingbox.views.utils import get_login_url, get_restricted_url, __get__title__, __matches__, reverse_lazy_query, \
    get_paginator_page, update_per_page_session_cookie


class ItemView(ElementView):
    model: Model = Item
    template_name: str = 'item/item.html'

    def get_view_title(self):
        return 'Item[%d]' % self.object.id


@require_POST
def post_search_items(request: HttpRequest) -> HttpResponse:
    name_query: str = request.POST.get('item_name')
    if len(name_query) < 1:
        messages.warning(request, '"name" parameter must contains at least one character.')
        name_query = '*'
    return HttpResponseRedirect(reverse_lazy_query('movingbox:search_items', query_kwargs={'name': name_query}))


@resident_required(login_url=get_login_url(), restricted_url=get_restricted_url())
@require_GET
def search_items(request: HttpRequest, display_all_items: bool = False) -> HttpResponse:
    cookie_per_page_items: str = 'items_per_page'
    cookie_per_page_furnitures: str = 'furnitures_per_page'
    cookie_per_page_objects: str = 'objects_per_page'
    per_page_attr: str = 'per_page'
    page_attr: str = 'page'

    is_paginated: bool = True
    display_room: bool = True
    display_header: bool = True

    title: str = __get__title__('Search Items/Furnitures')
    name_query: str = request.GET.get('name')
    display_all: bool = name_query == '*'

    if display_all or display_all_items:
        items = [item for item in Item.objects.all()]
        page_number: int = request.GET.get(page_attr, 1)

        if display_all_items:
            title = __get__title__('Items List')
            name_query = 'Items'
            furnitures = []
        else:
            cookie_per_page = 'objects_per_page'
            title = __get__title__('All Items/Furnitures')
            name_query = 'All Items and Furnitures'
            furnitures = [furniture for furniture in Furniture.objects.all()]

        per_page: int = update_per_page_session_cookie(request, per_page_attr, cookie_per_page_items)
        paginator: Paginator = Paginator(sort_items_furnitures(furnitures + items), per_page=per_page)
        paginator_page: Page = get_paginator_page(paginator, page_number)
        objects = paginator_page

    elif not name_query:
        is_paginated = False
        display_room = False
        display_header = False
        items = []
        furnitures = []
        name_query = ''
        messages.error(request, 'Invalid request. Requires "name" parameter.')

    else:
        if len(name_query) < 3:
            messages.warning(request, 'Please enter at least 3 characters for the search.')
        items: List[Item] = [item for item in Item.objects.all() if __matches__(name_query, item.name, False)]
        furnitures: List[Furniture] = [furniture for furniture in Furniture.objects.all() if
                                       __matches__(name_query, furniture.name, False)]

        items_per_page_attr: str = 'items_per_page'
        items_page_attr: str = 'items_page'
        furnitures_per_page_attr: str = 'furnitures_per_page'
        furnitures_page_attr: str = 'furnitures_page'

        items_page_number: int = request.GET.get(items_page_attr, 1)
        furnitures_page_number: int = request.GET.get(furnitures_page_attr, 1)

        per_page_items: int = update_per_page_session_cookie(request, items_per_page_attr, cookie_per_page_items)

        per_page_furnitures: int = update_per_page_session_cookie(request, furnitures_per_page_attr,
                                                                  cookie_per_page_furnitures)

        items_paginator: Paginator = Paginator(sort_items(items), per_page=per_page_items)
        items_paginator_page: Page = get_paginator_page(items_paginator, items_page_number)

        furnitures_paginator: Paginator = Paginator(sort_furnitures(furnitures), per_page=per_page_furnitures)
        furnitures_paginator_page: Page = get_paginator_page(furnitures_paginator, furnitures_page_number)

    return render(request, 'item/items.html', locals())


@resident_required(login_url=get_login_url(), restricted_url=get_restricted_url())
@require_GET
def all_items(request: HttpRequest) -> HttpResponse:
    return search_items(request, display_all_items=True)


class AddItemView(AddElementView):
    model: Model = Item
    fields: List[str] = ['name', 'box']
    element_name: str = 'Item'
    redirect_url: str = 'movingbox:get_item'
    cancel_url: str = 'movingbox:get_items'
    form_title: str = 'Add an Item'
    submit_button: str = 'Create Item'

    def get_redirect_args(self) -> List[Any]:
        return [self.object.id]


class UpdateItemView(UpdateElementView):
    model: Model = Item
    fields: List[str] = ['name', 'box']
    element_name: str = 'Item'
    redirect_url: str = 'movingbox:get_item'
    cancel_url: str = 'movingbox:get_item'
    form_title: str = 'Update an Item'
    submit_button: str = 'Save Item'

    def get_cancel_args(self) -> List[Any]:
        return [self.object.id]

    def get_redirect_args(self) -> List[Any]:
        return [self.object.id]


class DeleteItemView(DeleteElementView):
    model: Model = Item
    element_name: str = 'Item'
    redirect_url: str = 'movingbox:get_items'
    cancel_url: str = 'movingbox:get_item'
    form_title: str = 'Remove an Item'
    submit_button: str = 'Delete Item'

    def get_cancel_args(self) -> List[Any]:
        return [self.object.id]
