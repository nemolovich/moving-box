from typing import List, Any, Dict

from django.db.models import Model

from movingbox.models import Room
from movingbox.templatetags.filters import sort_rooms
from movingbox.views.generic_views import AddElementView, UpdateElementView, DeleteElementView, ElementView, \
    ElementsListView


class RoomView(ElementView):
    model: Model = Room
    template_name: str = 'room/room.html'

    def get_view_title(self):
        return 'Room %s ' % self.object.room_name


class RoomPrintView(RoomView):
    template_name: str = 'printer/room.html'


class RoomsView(ElementsListView):
    model: Model = Room
    template_name: str = 'room/rooms.html'
    context_object_name: str = 'rooms'
    view_title: str = 'Rooms list'

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context = super(RoomsView, self).get_context_data(**kwargs)
        context.update({
            'selected_room_id': int(self.request.GET.get('selected_room_id', sort_rooms(self.object_list)[0].id)),
        })
        return context


class AddRoomView(AddElementView):
    model: Model = Room
    fields: List[str] = ['room_name']
    element_name: str = 'Room'
    redirect_url: str = 'movingbox:get_room'
    cancel_url: str = 'movingbox:get_rooms'
    form_title: str = 'Add a Room'
    submit_button: str = 'Create Room'

    def get_redirect_args(self) -> List[Any]:
        return [self.object.id]


class UpdateRoomView(UpdateElementView):
    model: Model = Room
    fields: List[str] = ['room_name']
    element_name: str = 'Room'
    redirect_url: str = 'movingbox:get_room'
    cancel_url: str = 'movingbox:get_room'
    form_title: str = 'Update a Room'
    submit_button: str = 'Save Room'

    def get_cancel_args(self) -> List[Any]:
        return [self.object.id]

    def get_redirect_args(self) -> List[Any]:
        return [self.object.id]


class DeleteRoomView(DeleteElementView):
    model: Model = Room
    element_name: str = 'Room'
    redirect_url: str = 'movingbox:get_rooms'
    cancel_url: str = 'movingbox:get_room'
    form_title: str = 'Remove a Room'
    submit_button: str = 'Delete Room'

    def get_cancel_args(self) -> List[Any]:
        return [self.object.id]
