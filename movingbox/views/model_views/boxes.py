from typing import List, Any

from django.db.models import Model
from django.db.models.query import QuerySet

from movingbox.models import Box
from movingbox.views.generic_views import AddElementView, UpdateElementView, DeleteElementView, ElementView, \
    ElementsListView


class BoxView(ElementView):
    model: Model = Box
    template_name: str = 'box/box.html'

    def get_view_title(self):
        return 'Box #%d' % self.object.box_number


class BoxPrintView(BoxView):
    template_name: str = 'printer/box.html'


class BoxesView(ElementsListView):
    model: Model = Box
    template_name: str = 'box/boxes.html'
    context_object_name: str = 'boxes'
    view_title: str = 'Boxes list'
    queryset: QuerySet = Box.objects.order_by('box_number')
    cookie_per_page: str = 'boxes_per_page'
    is_paginated: bool = True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'display_room': True,
            'display_header': True,
        })
        return context


class AddBoxView(AddElementView):
    model: Model = Box
    fields: List[str] = ['room']
    element_name: str = 'Box'
    redirect_url: str = 'movingbox:get_box'
    cancel_url: str = 'movingbox:get_boxes'
    form_title: str = 'Add a Box'
    submit_button: str = 'Create Box'

    def get_redirect_args(self) -> List[Any]:
        return [self.object.box_number]


class UpdateBoxView(UpdateElementView):
    model: Model = Box
    fields: List[str] = ['room']
    element_name: str = 'Box'
    redirect_url: str = 'movingbox:get_box'
    cancel_url: str = 'movingbox:get_box'
    form_title: str = 'Update a Box'
    submit_button: str = 'Save Box'

    def get_form_title(self) -> str:
        return '%s (<b>%s</b>)' % (self.form_title, str(self.object))

    def get_cancel_args(self) -> List[Any]:
        return [self.object.box_number]

    def get_redirect_args(self) -> List[Any]:
        return [self.object.box_number]


class DeleteBoxView(DeleteElementView):
    model: Model = Box
    element_name: str = 'Box'
    redirect_url: str = 'movingbox:get_boxes'
    cancel_url: str = 'movingbox:get_box'
    form_title: str = 'Remove a Box'
    submit_button: str = 'Delete Box'

    def get_cancel_args(self) -> List[Any]:
        return [self.object.box_number]
