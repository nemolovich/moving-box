from typing import List, Any, Dict

from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Model
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.html import format_html
from django.views.generic import UpdateView, CreateView, DeleteView, DetailView, ListView

from movingbox.views.mixins import ResidentRequiredMixin, MoverRequiredMixin
from movingbox.views.utils import __get__title__, get_login_url, get_previous_url, update_per_page_session_cookie


class ElementView(MoverRequiredMixin, DetailView):
    model: Model = None
    login_url: str = get_login_url()
    redirect_field_name = 'target'
    template_name: str = None
    allow_empty: bool = False
    view_title: str = 'View Element'

    def get_view_title(self) -> str:
        return self.view_title

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context = super(ElementView, self).get_context_data(**kwargs)
        context.update({
            'title': __get__title__(self.get_view_title()),
        })
        return context


class ElementsListView(MoverRequiredMixin, ListView):
    model: Model = None
    login_url: str = get_login_url()
    redirect_field_name = 'target'
    template_name: str = None
    view_title: str = 'View Elements List'
    per_page_attr: str = 'per_page'
    page_attr: str = 'page'
    cookie_per_page: str = 'objects_per_page'
    is_paginated: bool = False

    def get_view_title(self) -> str:
        return self.view_title

    def get_paginate_by(self, queryset) -> int:
        if self.is_paginated:
            return update_per_page_session_cookie(self.request, self.per_page_attr, self.cookie_per_page)
        else:
            return None

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context = super(ElementsListView, self).get_context_data(**kwargs)
        context.update({
            'is_paginated': False,
            'title': __get__title__(self.get_view_title()),
        })
        if self.is_paginated:
            context.update({
                'is_paginated': True,
                'paginator_page': context['page_obj'],
                'per_page_attr': self.per_page_attr,
                'page_attr': self.page_attr,
            })
        return context


class EditElementView(ResidentRequiredMixin, SuccessMessageMixin):
    model: Model = None
    fields: List[str] = []
    template_name: str = 'edit_element.html'
    login_url: str = get_login_url()
    redirect_field_name = 'target'
    allow_empty: bool = False
    element_name: str = 'Element'
    redirect_url: str = 'movingbox:index'
    cancel_url: str = 'movingbox:index'
    form_title: str = 'Edit an Element'
    submit_button: str = 'Save Element'
    submit_icon: str = 'save'
    submit_color: str = 'info'
    back_to_previous: bool = True

    def get_redirect_args(self) -> List[Any]:
        return []

    def get_cancel_args(self) -> List[Any]:
        return []

    def get_success_message(self, _=None) -> str:
        return '%s successfully edited!' % self.element_name

    def resolve_redirect_url(self) -> str:
        return reverse_lazy(self.redirect_url, args=self.get_redirect_args())

    def resolve_cancel_url(self) -> str:
        return reverse_lazy(self.cancel_url, args=self.get_cancel_args())

    def get_success_url(self) -> str:
        return self.resolve_redirect_url()

    def get_cancel_url(self) -> str:
        return self.resolve_cancel_url()

    def get_previous_url(self) -> str:
        return get_previous_url(self.request, self.get_cancel_url())

    def get_form_title(self) -> str:
        return self.form_title

    def get_context(self) -> Dict[str, Any]:
        cancel_url: str = self.get_previous_url()
        return {
            'title': __get__title__(self.form_title),
            'cancel_url': cancel_url,
            'form_title': format_html(self.get_form_title()),
            'submit_button': self.submit_button,
            'submit_icon': self.submit_icon,
            'submit_color': self.submit_color,
        }


class AddElementView(EditElementView, CreateView):
    form_title: str = 'Add an Element'
    submit_button: str = 'Create Element'
    submit_icon: str = 'plus-square'
    submit_color: str = 'success'
    redirect_url: str = 'movingbox:index'
    cancel_url: str = 'movingbox:index'

    def get_success_message(self, _=None) -> str:
        return '%s successfully created!' % self.element_name

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context = super(AddElementView, self).get_context_data(**kwargs)
        context.update({
            **super(AddElementView, self).get_context(),
            'element_form': context['form'],
        })
        return context

    def get_initial(self) -> Dict[str, Any]:
        initial = super(AddElementView, self).get_initial()
        initial = {**initial, **self.kwargs}
        return initial


class UpdateElementView(EditElementView, UpdateView):
    form_title: str = 'Update an Element'
    submit_button: str = 'Save Element'
    submit_color: str = 'primary'
    extra_button: Dict[str, str] = None

    def get_extra_button(self) -> Dict[str, str]:
        return self.extra_button

    def get_success_message(self, _=None) -> str:
        return '%s successfully updated!' % self.element_name

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context = super(UpdateElementView, self).get_context_data(**kwargs)
        context.update({
            **super(UpdateElementView, self).get_context(),
            'element_form': context['form'],
            'extra_button': self.get_extra_button(),
        })
        return context


class DeleteElementView(EditElementView, DeleteView):
    form_title: str = 'Delete an Element'
    submit_button: str = 'Delete Element'
    submit_icon: str = 'trash'
    submit_color: str = 'danger'

    def get_confirmation_msg(self) -> str:
        return format_html('Are you sure you want to delete the <i>%s</i> "<b>%s</b>"?' %
                           (self.element_name, str(self.object)))

    def get_success_message(self, _=None) -> str:
        return '%s successfully removed!' % self.element_name

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context = super(DeleteElementView, self).get_context_data(**kwargs)
        context.update({
            **super(DeleteElementView, self).get_context(),
            'confirmation_msg': self.get_confirmation_msg(),
            'delete_element': True,
        })
        return context

    def delete(self, request, *args, **kwargs) -> HttpResponseRedirect:
        response: HttpResponseRedirect = super().delete(self, request, args, kwargs)
        messages.success(self.request, self.get_success_message())
        return response
