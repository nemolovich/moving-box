from typing import List

from django.contrib import messages
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Model
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.html import format_html
from django.views.decorators.http import require_http_methods, require_GET, require_POST
from django.views.generic import UpdateView, TemplateView

from movingbox.models import AppUser
from movingbox.views.utils import get_index_url, get_login_url, __get__title__, update_password, get_app_setting, \
    get_previous_url


class AccountView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model: Model = AppUser
    fields: List[str] = ['first_name', 'last_name', 'email']
    login_url: str = get_login_url()
    redirect_field_name = 'target'
    template_name: str = 'account/account.html'
    allow_empty: bool = False
    success_url: str = get_index_url()
    success_message: str = 'Account successfully updated!'

    def get_previous_url(self) -> str:
        previous_url: str = get_previous_url(self.request)

        if previous_url == reverse_lazy('movingbox:update_password', args=[self.object.id]):
            previous_url = self.success_url

        return previous_url

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'title': __get__title__('Update Account'),
            'user': self.object,
            'account_form': context['form'],
            'cancel_url': self.get_previous_url(),
        })
        return context


class IndexView(TemplateView):
    template_name: str = 'index.html'
    context_object_name: str = 'index'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'title': __get__title__(),
            'home_image': get_app_setting('app_home_image', ''),
            'home_title': format_html(get_app_setting('app_home_title', 'Title (APP_HOME_TITLE)')),
            'home_message': format_html(get_app_setting('app_home_message', 'Message (APP_HOME_MESSAGE)')),
        })
        return context


@require_GET
def get_login(request: HttpRequest) -> HttpResponse:
    title: str = __get__title__('Login')
    target_url: str = request.GET.get('target') or ''
    return render(request, 'account/login.html', locals())


@require_GET
def get_restricted(request: HttpRequest) -> HttpResponse:
    title: str = __get__title__('Access Restricted')
    return render(request, 'restricted.html', locals())


@require_POST
def post_login(request: HttpRequest) -> HttpResponse:
    username = request.POST.get('user_login')
    password = request.POST.get('user_password')
    target = request.POST.get('target_url') or ''
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        messages.success(request, 'Login succeed')
        redirect_url: str = target if len(target) else get_index_url()
        return HttpResponseRedirect(redirect_url)
    else:
        messages.error(request, 'Login failed')
        return HttpResponseRedirect(get_login_url())


@login_required(login_url=get_login_url(), redirect_field_name='target')
@require_GET
def get_logout(request: HttpRequest) -> HttpResponse:
    logout(request)
    messages.success(request, 'Logout succeed')
    return HttpResponseRedirect(get_index_url())


@login_required(login_url=get_login_url(), redirect_field_name='target')
@require_http_methods(["GET", "POST"])
def update_account_password(request: HttpRequest, pk: int) -> HttpResponse:
    return update_password(request, pk, account_edit=True)
