from django.apps import AppConfig


class MovingboxConfig(AppConfig):
    name = 'movingbox'
