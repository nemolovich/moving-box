from typing import List, Dict, Optional

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import Group
from django.db.models import Model
from django.urls import reverse_lazy
from django.utils.html import format_html

from movingbox.models import AppUser, Furniture
from movingbox.views.utils import build_url
from .models import Room, Box, Item


class AppAdminSite(admin.AdminSite):
    site_title = 'Moving Box'
    site_header = 'Moving Box Administration'


admin_site = AppAdminSite(name='app_admin')
admin_site.site_url = "/movingbox"


def get_box_admin_url(box: Box) -> str:
    url: str = reverse_lazy_query("app_admin:movingbox_box_changelist", query_kwargs={'q': str(box.box_number)})
    nb: int = box.box_number
    return format_html(u'<a href="%(url)s">Box #%(nb)d</a>' % locals())


def get_room_admin_url(room: Room) -> str:
    url: str = reverse_lazy_query("app_admin:movingbox_room_changelist", query_kwargs={'q': str(room.room_name)})
    room_name: str = room.room_name
    return format_html(u'<a href="%(url)s">%(room_name)s</a>' % locals())


class ItemInline(admin.TabularInline):
    model: Model = Item
    extra: int = 0


class BoxInline(admin.StackedInline):
    model: Model = Box
    extra: int = 0


@admin.register(Room, site=admin_site)
class RoomAdmin(admin.ModelAdmin):
    fields: List[str] = ['room_name']
    list_display = ['room_name']
    inlines = [BoxInline]
    search_fields = ['room_name']


@admin.register(Box, site=admin_site)
class BoxAdmin(admin.ModelAdmin):
    fields: List[str] = ['room']
    inlines = [ItemInline]
    list_display = ['_box_', '_room_']
    list_filter = ['room']
    ordering = ['box_number']
    search_fields = ['box_number']

    @staticmethod
    def _box_(obj: Box):
        return 'Box #%d' % obj.box_number

    @staticmethod
    def _room_(obj: Box):
        return get_room_admin_url(obj.room)


@admin.register(Item, site=admin_site)
class ItemAdmin(admin.ModelAdmin):
    fields: List[str] = ['name', 'box']
    list_display = ['name', '_box_']
    list_filter = ['box']
    ordering = ['box__box_number', 'name']
    search_fields = ['name']

    @staticmethod
    def _box_(obj: Item):
        return get_box_admin_url(obj.box)


@admin.register(Furniture, site=admin_site)
class FurtnitureAdmin(admin.ModelAdmin):
    fields: List[str] = ['name', 'room']
    list_display = ['name', '_room_']
    list_filter = ['room']
    ordering = ['name']
    search_fields = ['name']

    @staticmethod
    def _room_(obj: Item):
        return get_room_admin_url(obj.room)


admin_site.register(AppUser, UserAdmin)
admin_site.register(Group, GroupAdmin)
