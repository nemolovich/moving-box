from typing import List

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Field, ForeignKey, QuerySet

ROLES: List[str] = ['Admin', 'Resident', 'Mover']


class AppUser(AbstractUser):

    def is_mover(self) -> bool:
        return self.groups.filter(name='Mover').exists()

    def is_resident(self) -> bool:
        return self.groups.filter(name='Resident').exists()

    def is_admin(self) -> bool:
        return self.groups.filter(name='Admin').exists()

    class Meta:
        permissions = [
            ("can_update_admin", "Can update admin"),
            ("can_delete_admin", "Can delete admin"),
        ]


class Room(models.Model):
    id: QuerySet
    room_name: Field = models.CharField(max_length=50)
    room_name.short_description = 'The name of the room'
    boxes: QuerySet

    def __str__(self) -> str:
        return str(self.room_name)

    def __to_string__(self) -> str:
        return 'Room{room_name=%s}' % self.room_name

    def __info__(self) -> str:
        return 'Room{room_name=%s, boxes=%s}' % (self.room_name, [box.__info__() for box in self.boxes.all()])


class Box(models.Model):
    box_number: Field = models.AutoField(primary_key=True)
    box_number.short_description = 'The number of this box'
    room: ForeignKey = models.ForeignKey(Room, related_name='boxes', on_delete=models.DO_NOTHING)
    room.short_description = 'The room in which this box is located'
    items: QuerySet

    def __str__(self) -> str:
        return 'Box #%d' % self.box_number

    def __to_string__(self) -> str:
        return 'Box{box_number=%s, room=%s}' % (self.box_number, self.room)

    def __info__(self) -> str:
        return 'Box{box_number=%s, items=%s}' % (self.box_number, map(Item.__info__, self.items.all()))


class Item(models.Model):
    id: QuerySet
    name: Field = models.CharField(max_length=200)
    name.short_description = 'The name of this item'
    box: ForeignKey = models.ForeignKey(Box, related_name='items', on_delete=models.DO_NOTHING)
    box.short_description = 'The box containing this item'

    def __str__(self) -> str:
        return str(self.name)

    def __to_string__(self) -> str:
        return 'Item{name=%s, box=%s}' % (self.name, self.box)

    def __info__(self) -> str:
        return 'Item{name=%s}' % self.name


class Furniture(models.Model):
    id: QuerySet
    name: Field = models.CharField(max_length=200)
    name.short_description = 'The name of this furniture'
    room: ForeignKey = models.ForeignKey(Room, related_name='furnitures', on_delete=models.DO_NOTHING)
    room.short_description = 'The room in which this furniture is located'

    def __str__(self) -> str:
        return str(self.name)

    def __to_string__(self) -> str:
        return 'Furniture{name=%s, room=%s}' % (self.name, self.room)

    def __info__(self) -> str:
        return 'Furniture{name=%s}' % self.name
