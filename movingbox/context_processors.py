import copy
import re
from typing import List, Any, Dict, Optional, Callable, Tuple, AnyStr

import yaml
from django.apps import apps
from django.db.models import Model
from django.http import HttpRequest
from django.urls import reverse

from movingbox.models import ROLES, AppUser
from movingbox.settings import RELOAD_SITEMAP
from movingbox.sitemap import SitemapItem, SitemapButton
from movingbox.templatetags.filters import get_group, flat_map, flat_list
from movingbox.views.utils import get_app_settings, get_previous_url, build_url

NAME_KEY: str = 'name'
URL_KEY: str = 'url'
URL_MATCH_KEY: str = 'url_match'
ELEMENTS_KEY: str = 'elements'
BUTTONS_KEY: str = 'buttons'
BUTTONS_DISABLED_KEY: str = 'disable_button'

BUTTON_VALUE_KEY: str = 'value'
BUTTON_URL_KEY: str = 'url'
BUTTON_CONDITION_KEY: str = 'condition'
BUTTON_CONFIG_KEY: str = 'config'

__SITEMAP__: List[SitemapItem] = []


def __get_url_without_param__(url: str) -> str:
    return '/'.join(re.sub(r'\d+', '\\\\d+', path) for path in url.split('/'))


def __reverse_without_param__(url: str, args=None) -> str:
    if args is None:
        args = [0]
    return __get_url_without_param__(reverse(url, args=args))


def __get_url_params__(url: str) -> List[int]:
    ids: List[str] = re.findall(r'(\d+)', url)
    if len(ids) > 0:
        return list(map(int, ids))
    return []


def is_admin(user: AppUser) -> bool:
    return user.is_authenticated and user.is_admin()


def is_resident(user: AppUser) -> bool:
    return user.is_authenticated and user.is_resident()


def is_mover(user: AppUser) -> bool:
    return user.is_authenticated and user.is_mover()


def is_greater_role(user1: AppUser, user2: AppUser) -> bool:
    return user2 is None or ROLES.index(get_group(user1)) < ROLES.index(get_group(user2))


def __resolve_name__(sitemap_item: SitemapItem and SitemapButton, context: Dict[str, Any]) -> str:
    result: str = sitemap_item.name
    req_args: List[Any] = context['request_args']
    if re.match(r'^format:', sitemap_item.name) is not None and len(req_args) > 0:
        result = re.sub(r'^format:', '', sitemap_item.name) % \
                 (*[req_args[arg] for arg in sitemap_item.url_args],)
    to_eval: re.Match = re.search(
        r'^eval:(?P<before>.*)(?P<content>\{(?P<model>\w+)\[(?P<arg>\d+)\]__(?P<attribute>\w+)\})(?P<after>.*)',
        sitemap_item.name)
    if to_eval is not None and len(req_args) >= len(sitemap_item.url_args):
        model: Model = apps.get_model(app_label='movingbox', model_name=to_eval.group('model'))
        arg: int = req_args[int(to_eval.group('arg'))]
        attribute: str = to_eval.group('attribute')
        try:
            element: Any = model.objects.get(pk=arg)
            result = to_eval.group('before') + str(getattr(element, attribute)) + to_eval \
                .group('after')
        except model.DoesNotExist:
            pass
    return result


def __resolve_kwarg__(url_kwarg: str, context: Dict[str, Any]) -> Optional[str]:
    value_match: re.Match = re.search(r'^value:\{(?P<value>\w+)\}$', url_kwarg)
    arg_match: re.Match = re.search(r'^arg:\{(?P<arg_number>\d+)\}$', url_kwarg)
    res: Optional[str] = url_kwarg
    if value_match is not None:
        value: str = value_match.group('value')
        if value == 'Any':
            res = None
        else:
            res = value
    elif arg_match is not None:
        arg_number = arg_match.group('arg_number')
        res = context['request_args'][int(arg_number)]
    return res


def __resolve_url__(sitemap_item: SitemapItem and SitemapButton, context: Dict[str, Any]) -> str:
    args: Optional[List[Any]] = None
    if len(sitemap_item.url_args) > 0:
        req_args: List[int] = context['request_args']
        args: List[int] = [req_args[arg] for arg in sitemap_item.url_args]
    url: str = reverse(sitemap_item.url, args=args)
    if len(sitemap_item.url_kwargs) > 0:
        url_kwargs: Dict[str, str] = {}
        for k, v in sitemap_item.url_kwargs.items():
            value = __resolve_kwarg__(v, context)
            if value is not None:
                url_kwargs[k] = value
        url = build_url(url, url_kwargs)
    return url


def __resolve_url_match__(sitemap_item: SitemapItem and SitemapButton, context: Dict[str, Any]) -> str:
    match_url: str = sitemap_item.url_match
    reverse_rgx: re.Pattern[str] = r'^reverse:\{(?P<path>movingbox:\w+)\}(?P<end_path>.*)$'
    reverse_match: re.Match = re.search(reverse_rgx, match_url)
    if reverse_match is not None:
        path: str = reverse_match.group('path')
        args: Optional[List[Any]] = None
        if len(sitemap_item.url_args) > 0:
            req_args: List[int] = context['request_args']
            args: List[int] = [req_args[arg] for arg in sitemap_item.url_args]
        match_url = re.sub(reverse_rgx, r'' + reverse(path, args=args) + r'\g<end_path>', match_url)
    return match_url


def __resolve_sitemap_urls__(sitemap: List[SitemapItem], context: Dict[str, Any]) -> List[SitemapItem]:
    new_sitemap: List[SitemapItem] = [copy.deepcopy(s) for s in sitemap[:]]
    for sitemap_item in new_sitemap:
        if not sitemap_item.resolved:
            sitemap_item.name = __resolve_name__(sitemap_item, context)
            sitemap_item.url = __resolve_url__(sitemap_item, context)
            sitemap_item.url_match = __resolve_url_match__(sitemap_item, context)
            sitemap_item.resolved = True
        if len(sitemap_item.elements) > 0:
            sitemap_item.elements = __resolve_sitemap_urls__(sitemap_item.elements, context)
        for button in sitemap_item.buttons:
            if not button.resolved:
                button.name = __resolve_name__(button, context)
                button.url = __resolve_url__(button, context)
                button.url_match = __resolve_url_match__(sitemap_item, context)
                button.resolved = True
    return new_sitemap


def __load_sitemap__() -> None:
    global __SITEMAP__
    with open('movingbox/sitemap/sitemap.yml', 'r') as yml_file:
        __SITEMAP__ = yaml.load(yml_file, Loader=yaml.UnsafeLoader) or []


def __get_sitemap__() -> List[SitemapItem]:
    global __SITEMAP__
    if len(__SITEMAP__) < 1 or RELOAD_SITEMAP:
        __load_sitemap__()
    return __SITEMAP__


def add_variable_to_context(request: HttpRequest):
    app_settings: Dict[str, str] = get_app_settings()
    request_args: List[int] = __get_url_params__(request.path)
    request_args = [request_args[x] if len(request_args) > x else 0 for x in range(10)]
    request_kwargs: Dict[str, str] = {x: request.GET[x] for x in request.GET}
    current_path: str = build_url(request.path, request_kwargs)
    name_key: str = NAME_KEY
    url_key: str = URL_KEY
    url_match_key: str = URL_MATCH_KEY
    elements_key: str = ELEMENTS_KEY
    buttons_key: str = BUTTONS_KEY
    arg_user: Optional[AppUser]
    previous_url: str = get_previous_url(request)
    try:
        arg_user = AppUser.objects.get(id=request_args[0])
    except AppUser.DoesNotExist:
        arg_user = None
    req_user: AppUser = request.user

    button_conditions: Dict[str, Callable[[], bool]] = {
        'never': lambda: False,
        'always': lambda: True,
        'is_mover': lambda: is_mover(req_user),
        'is_resident': lambda: is_resident(req_user),
        'is_admin': lambda: is_admin(req_user),
        'is_greater_role': lambda: is_greater_role(req_user, arg_user),
    }

    sitemap: List[SitemapItem] = __resolve_sitemap_urls__(__get_sitemap__(), locals())
    return locals()
