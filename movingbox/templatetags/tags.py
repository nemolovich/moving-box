import re
from typing import Dict, Any, List

from django.template.defaulttags import register


@register.simple_tag
def to_list(*args):
    return args


@register.simple_tag
def sanitize(value: str) -> str:
    if re.match(r'^http://[^:]+:443/.*$', value) is not None:
        return value.replace('http://', 'https://').replace(':443', '')
    return value


@register.simple_tag
def get_or_else(value: Any, default_value: Any) -> Any:
    return value if value is not None and len(str(value)) else default_value


@register.simple_tag
def sub_dict(value: Dict[str, Any], *keys: List[str]) -> Dict[str, Any]:
    res = {**value}
    for key in keys:
        if key in res:
            res.pop(key)
    return res
