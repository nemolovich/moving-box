import itertools
import re
from typing import List, Any, Dict, Callable, Optional

from django.contrib.auth.models import Group
from django.template.defaultfilters import register
from unidecode import unidecode

from movingbox.models import Item, Box, AppUser, ROLES, Furniture, Room
from movingbox.sitemap import SitemapItem, SitemapButton
from movingbox.views.utils import build_url


@register.filter
def sort_rooms(rooms: List[Room], reverse: bool = False) -> List[Room]:
    return sorted(rooms, key=lambda r: unidecode(r.room_name).lower(), reverse=reverse)


@register.filter
def sort_boxes(boxes: List[Box], reverse: bool = False) -> List[Box]:
    return sorted(boxes, key=lambda b: b.box_number, reverse=reverse)


@register.filter
def sort_items(items: List[Item], reverse: bool = False) -> List[Item]:
    return sort_items_furnitures(items, reverse)


@register.filter
def sort_furnitures(furnitures: List[Furniture], reverse: bool = False) -> List[Furniture]:
    return sort_items_furnitures(furnitures, reverse)


@register.filter
def sort_items_furnitures(objects: List[Item or Furniture], reverse: bool = False) -> List[Item or Furniture]:
    return sorted(objects, key=lambda o: unidecode(o.name).lower(), reverse=reverse)


@register.filter
def sort_users(users: List[AppUser], reverse: bool = False) -> List[AppUser]:
    return sorted(users, key=lambda u: (ROLES.index(get_group(u)), unidecode(u.first_name).lower()), reverse=reverse)


@register.filter
def get_group(user: AppUser) -> str:
    return sort_groups(user.groups.all())[0].name


@register.filter
def sort_groups(groups: List[Group]) -> List[Group]:
    return sorted(groups, key=lambda g: ROLES.index(g.name))


@register.filter
def get_item(value, arg):
    try:
        return value[arg]
    except KeyError:
        return ''


@register.filter
def get_index(value, arg):
    return list(value).index(arg)


@register.filter
def match(value, arg, opt=None):
    has_match: bool = re.match(r'^' + value + r'$', arg) is not None
    if opt is not None:
        has_match |= re.match(r'^' + value + r'$', opt) is not None
    return has_match


@register.filter
def first_match(value, args):
    arg: str = args[0]
    key: str = args[1]
    opt: Optional[str] = None
    if len(args) > 2:
        opt = args[2]
    return next(v for v in value if match(v[key], arg, opt))


@register.filter
def any_match(value, arg) -> bool:
    return any([match(v, arg) for v in value])


@register.filter
def flat_map(value: List[Dict or SitemapItem], args: List[Any]) -> List[Dict]:
    arg: str = args[0]
    keys: List[str] = args[1]
    parents_key: List[List[str]] = []
    if len(args) > 2:
        parents_key = args[2]
    res: List[Dict] = []
    for item in value:
        if not isinstance(item, dict):
            try:
                item: Dict[str: Any] = {**item.__to_dict__()}
            except AttributeError:
                item: Dict[str: Any] = {**item.__dict__}
        d = {**item}
        if arg in d:
            d.pop(arg)
        res += [{**d, 'parents': parents_key}]
        if arg in item and len(item[arg]) > 0:
            res += flat_map(item[arg], [arg, keys, parents_key + [[item[key] for key in keys]]])
    return res


@register.filter
def get_items(value: List[Dict], args: List[Any]) -> List[Dict]:
    params: List[str] = [args[0]]
    keys: List[str] = args[1]
    if len(args) > 2:
        params += args[2:]
    return [v for v in value if [str(v[param]) for param in params] in keys]


@register.filter
def get_key_list(value: List[Dict], arg: str) -> List[Any]:
    return [v[arg] for v in value if arg in v]


@register.filter
def concat_item(value, arg):
    return value + [arg]


@register.filter
def flat_list(value):
    return list(itertools.chain(*value))


@register.filter
def concat(value, arg):
    return str(value) + str(arg)


@register.filter
def substract(value, arg):
    return int(value) - int(arg)


@register.filter
def resolve_condition(value: str, arg: Dict[str, Callable[[], bool]]):
    resolve: Callable[[str], bool] = lambda key: arg[key]()
    or_blocks: List[str] = [value]
    if '||' in value:
        or_blocks = [v.strip() for v in value.split('||')]

    or_ok: bool = False

    for block in or_blocks:
        and_blocks: List[str] = [block]
        if '&&' in block:
            and_blocks: List[str] = [v.strip() for v in block.split('&&')]

        and_ok: bool = True

        for condition in and_blocks:
            if not resolve(condition):
                and_ok = False
                break

        or_ok = or_ok or and_ok

    return or_ok


@register.filter
def url_kwargs(path: str, kwarg: Dict[str, Any]) -> str:
    return build_url(path, kwarg)


@register.filter
def get_url_separator(kwarg: Dict[str, Any]) -> str:
    return '&' if len(kwarg) > 0 else '?'
