from django.test import TestCase

from movingbox.models import Room, Box, Item


class RoomModelTests(TestCase):

    def test_create_room(self):
        room_name: str = "TestRoom"
        room: Room = Room(room_name=room_name)
        self.assertIs(room_name, room.room_name)


class BoxModelTests(TestCase):

    def test_create_box(self):
        room_name: str = "TestRoom"
        room = Room(room_name=room_name)
        box_nb: int = 42
        box: Box = Box(box_number=box_nb, room=room)
        self.assertIs(box_nb, box.box_number)
        self.assertIs(room, box.room)


class ItemModelTests(TestCase):

    def test_create_room(self):
        room_name: str = "TestRoom"
        room = Room(room_name=room_name)
        box_nb: int = 42
        box: Box = Box(box_number=box_nb, room=room)
        item_name: str = "Item Test"
        item: Item = Item(name=item_name, box=box)
        self.assertIs(item_name, item.name)
        self.assertIs(box, item.box)
        self.assertIs(room, item.box.room)
