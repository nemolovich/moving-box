from app.settings.base import env

RELOAD_SITEMAP = env('RELOAD_SITEMAP', default=False)

APP_NAME = env('APP_NAME', default='Moving Box')
APP_HOME_IMAGE = env('APP_HOME_IMAGE', default='home_img.jpg')
APP_HOME_TITLE = env('APP_HOME_TITLE', default='Take charge of your move!')
APP_HOME_MESSAGE = env('APP_HOME_MESSAGE', default='Welcome to <b>Moving Box</b> Application!')

BOTTOM_TEXT = 'V1.0.3'
DEFAULT_PER_PAGE = 10
PER_PAGE_VALUES = [5, 10, 20, 50, 100]
