from app.settings.base import *
from movingbox.settings.default import *

APP_NAME = env('APP_NAME', default='Moving Box')
APP_HOME_IMAGE = env('APP_HOME_IMAGE', default='home_img.jpg')
APP_HOME_TITLE = env('APP_HOME_TITLE', default='Take charge of your move!')
APP_HOME_MESSAGE = env('APP_HOME_MESSAGE', default='Welcome to <b>Moving Box</b> Application!')
