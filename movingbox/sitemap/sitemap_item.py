from typing import List, Any, Dict

from movingbox.sitemap.sitemap_button import SitemapButton


class SitemapItem:
    name: str
    url: str
    url_match: str
    url_args: List[int] = []
    url_kwargs: Dict[str, str] = {}
    elements: List['SitemapItem'] = []
    buttons: List[SitemapButton] = []
    disable_buttons: bool = False
    resolved: bool = False

    @staticmethod
    def new(name: str, url: str, url_match: str, url_args: List[int] = None, url_kwargs: Dict[str, str] = None,
            elements: List['SitemapItem'] = None, buttons: List[SitemapButton] = None, disable_buttons: bool = False,
            resolved: bool = False):
        if url_kwargs is None:
            url_kwargs = {}
        if url_args is None:
            url_args = []
        if elements is None:
            elements = []
        if buttons is None:
            buttons = []
        sitemap: SitemapItem = SitemapItem()
        sitemap.name = name
        sitemap.url = url
        sitemap.url_match = url_match
        sitemap.url_args = url_args
        sitemap.url_kwargs = url_kwargs
        sitemap.elements = elements
        sitemap.buttons = buttons
        sitemap.disable_buttons = disable_buttons
        sitemap.resolved = resolved
        return sitemap

    def to_string(self, level: int = 0):
        return '%(space)sSitemapItem{\n' \
               '%(space)s\tname: %(name)s,\n' \
               '%(space)s\turl: %(url)s,\n' \
               '%(space)s\turl_match: %(url_match)s,\n' \
               '%(space)s\turl_args: %(url_args)s,\n' \
               '%(space)s\turl_kwargs: %(url_kwargs)s,\n' \
               '%(space)s\telements: [\n%(elements)s%(space)s\t],\n' \
               '%(space)s\tbuttons: [\n%(buttons)s%(space)s\t],\n' \
               '%(space)s\tdisable_buttons: %(disable_buttons)s\n' \
               '%(space)s\tresolved: %(resolved)s\n' \
               '%(space)s}' % {
                   'space': '\t' * level,
                   'name': self.name,
                   'url': self.url,
                   'url_match': self.url_match,
                   'url_args': '[%s]' % ', '.join(map(str, self.url_args)),
                   'url_kwargs': self.url_kwargs,
                   'elements': ',\n'.join(element.to_string(level + 1) for element in self.elements),
                   'buttons': ',\n'.join(button.to_string(level + 1) for button in self.buttons),
                   'disable_buttons': self.disable_buttons,
                   'resolved': self.resolved,
               }

    def __repr__(self):
        return self.to_string()

    def __to_dict__(self) -> Dict[str, Any]:
        return {
            'name': self.name,
            'url': self.url,
            'url_match': self.url_match,
            'url_args': self.url_args,
            'url_kwargs': self.url_kwargs,
            'elements': [element.__to_dict__() for element in self.elements],
            'buttons': [button.__to_dict__() for button in self.buttons],
            'disable_buttons': self.disable_buttons,
            'resolved': self.resolved,
        }
