from typing import List, Any, Dict


class SitemapButton:
    name: str
    url: str
    url_match: str
    url_args: List[int] = []
    url_kwargs: Dict[str, str] = {}
    condition: str = 'always'
    config: Dict[str, Any] = {}
    resolved: bool = False

    @staticmethod
    def new(name: str, url: str, url_match: str, url_args: List[int] = None, url_kwargs: Dict[str, str] = None,
            condition: str = 'always', config: Dict[str, Any] = None, resolved: bool = False):
        if url_args is None:
            url_args = []
        if url_kwargs is None:
            url_kwargs = {}
        if config is None:
            config = {}
        button: SitemapButton = SitemapButton()
        button.name = name
        button.url = url
        button.url_match = url_match
        button.url_args = url_args
        button.url_kwargs = url_kwargs
        button.condition = condition
        button.config = config
        button.resolved = resolved
        return button

    def to_string(self, level: int = 0):
        return '%(space)sSitemapButton{\n' \
               '%(space)s\tname: %(name)s,\n' \
               '%(space)s\turl: %(url)s,\n' \
               '%(space)s\turl_match: %(url_match)s,\n' \
               '%(space)s\turl_args: %(url_args)s,\n' \
               '%(space)s\turl_kwargs: %(url_kwargs)s,\n' \
               '%(space)s\tcondition: [%(condition)s],\n' \
               '%(space)s\tconfig: %(config)s,\n' \
               '%(space)s\tresolved: %(resolved)s,\n' \
               '%(space)s}' % {
                   'space': '\t' * level,
                   'name': self.name,
                   'url': self.url,
                   'url_match': self.url_match,
                   'url_args': '[%s]' % ', '.join(map(str, self.url_args)),
                   'url_kwargs': self.url_kwargs,
                   'condition': self.condition,
                   'config': self.config,
                   'resolved': self.resolved,
               }

    def __repr__(self):
        return self.to_string()

    def __to_dict__(self) -> Dict[str, Any]:
        return {
            'name': self.name,
            'url': self.url,
            'url_match': self.url_match,
            'url_args': self.url_args,
            'url_kwargs': self.url_kwargs,
            'condition': self.condition,
            'config': self.config,
            'resolved': self.resolved,
        }
