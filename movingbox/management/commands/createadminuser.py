import logging

from django.contrib.auth.management.commands import createsuperuser
from django.contrib.auth.models import Group
from django.core.management import CommandError

ADMIN_GROUP: str = 'Admin'


class Command(createsuperuser.Command):
    help = 'Create an administrator user with provided password'

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        parser.add_argument(
            '--password', dest='password', default=None,
            help='Specifies the password for the administrator.',
        )

    def handle(self, *args, **options):
        password = options.get('password')
        username = options.get('username')
        database = options.get('database')

        if password and not username:
            raise CommandError("--username is required if specifying --password")

        super(Command, self).handle(*args, **options)

        if password:
            user = self.UserModel._default_manager.db_manager(database).get(username=username)
            user.set_password(password)
            user.save()

            try:
                admin_group = Group.objects.get(name=ADMIN_GROUP)
                if admin_group:
                    print('Add administrator to group \'%s\'' % ADMIN_GROUP)
                    user.groups.add(admin_group)
                    user.save()
            except Group.DoesNotExist:
                logging.warning('Group not found with name \'%s\'.' % ADMIN_GROUP)
