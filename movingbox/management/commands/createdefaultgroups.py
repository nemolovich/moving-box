"""
Create default groups for Moving Box.
"""
import logging

from django.contrib.auth.models import Group, Permission
from django.core.management.base import BaseCommand

GROUPS = {
    'Admin': [
        "Can add box",
        "Can change box",
        "Can delete box",
        "Can view box",
        "Can add room",
        "Can change room",
        "Can delete room",
        "Can view room",
        "Can add item",
        "Can change item",
        "Can delete item",
        "Can view item",
        "Can add furniture",
        "Can change furniture",
        "Can delete furniture",
        "Can view furniture",
        "Can add app user",
        "Can change app user",
        "Can delete app user",
        "Can view app user",
        "Can update admin",
        "Can delete admin",
        "Can add log entry",
        "Can change log entry",
        "Can delete log entry",
        "Can view log entry",
        "Can add permission",
        "Can change permission",
        "Can delete permission",
        "Can view permission",
        "Can add group",
        "Can change group",
        "Can delete group",
        "Can view group",
        "Can add content type",
        "Can change content type",
        "Can delete content type",
        "Can view content type",
        "Can add session",
        "Can change session",
        "Can delete session",
        "Can view session",
    ],
    'Resident': [
        "Can add box",
        "Can change box",
        "Can delete box",
        "Can view box",
        "Can add room",
        "Can change room",
        "Can delete room",
        "Can view room",
        "Can add item",
        "Can change item",
        "Can delete item",
        "Can view item",
        "Can add furniture",
        "Can change furniture",
        "Can delete furniture",
        "Can view furniture",
        "Can add app user",
        "Can change app user",
        "Can delete app user",
        "Can view app user",
        "Can view group",
    ],
    'Mover': [
        "Can view box",
        "Can view room",
        "Can view item",
        "Can view app user",
        "Can view furniture",
        "Can view group",
    ],
}


class Command(BaseCommand):
    help = 'Create default groups for Moving Box.'

    def handle(self, *args, **options):
        for group_name in GROUPS:
            new_group, is_created = Group.objects.get_or_create(name=group_name)
            for permission_name in GROUPS[group_name]:
                print('Add \'%s\' to group \'%s\'' % (permission_name, new_group.name))

                try:
                    permission_ = Permission.objects.get(name=permission_name)
                except Permission.DoesNotExist:
                    logging.warning('Permission not found with name \'%s\'.' % permission_name)
                    continue

                new_group.permissions.add(permission_)

        print('Default groups and permissions created.')
