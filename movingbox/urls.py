from django.urls import path, include

from movingbox import views

app_name = 'movingbox'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('restricted', views.get_restricted, name='restricted'),
    path('account/', include([
        path('<int:pk>/', views.AccountView.as_view(), name='account'),
        path('<int:pk>/update_password', views.update_account_password, name='update_password'),
        path('login', views.get_login, name='login'),
        path('submit_login', views.post_login, name='post_login'),
        path('logout', views.get_logout, name='logout'),
    ])),
    path('rooms/', include([
        path('', views.RoomsView.as_view(), name='get_rooms'),
        path('<int:pk>/', views.RoomView.as_view(), name='get_room'),
        path('add/', views.AddRoomView.as_view(), name='add_room'),
        path('<int:pk>/update/', views.UpdateRoomView.as_view(), name='update_room'),
        path('<int:pk>/delete/', views.DeleteRoomView.as_view(), name='delete_room'),
        path('<int:room>/box/<int:pk>/', views.BoxView.as_view(), name='get_room_box'),
        path('<int:room>/box/<int:room_number>/item/<int:pk>/', views.ItemView.as_view(), name='get_room_box_item'),
        path('<int:room>/furniture/<int:pk>/', views.FurnitureView.as_view(), name='get_room_furniture'),
    ])),
    path('boxes/', include([
        path('', views.BoxesView.as_view(), name='get_boxes'),
        path('<int:pk>/', views.BoxView.as_view(), name='get_box'),
        path('add/', views.AddBoxView.as_view(), name='add_box'),
        path('add/<int:room>/', views.AddBoxView.as_view(), name='add_box_to_room'),
        path('<int:pk>/update/', views.UpdateBoxView.as_view(), name='update_box'),
        path('<int:pk>/delete/', views.DeleteBoxView.as_view(), name='delete_box'),
        path('<int:box_number>/item/<int:pk>/', views.ItemView.as_view(), name='get_box_item'),
    ])),
    path('items/', include([
        path('', views.all_items, name='get_items'),
        path('<int:pk>/', views.ItemView.as_view(), name='get_item'),
        path('add/', views.AddItemView.as_view(), name='add_item'),
        path('add/<int:box>/', views.AddItemView.as_view(), name='add_item_to_box'),
        path('<int:pk>/update/', views.UpdateItemView.as_view(), name='update_item'),
        path('<int:pk>/delete/', views.DeleteItemView.as_view(), name='delete_item'),
        path('search', views.post_search_items, name='post_search_items'),
        path('result', views.search_items, name='search_items'),
    ])),
    path('furnitures/', include([
        path('', views.FurnituresView.as_view(), name='get_furnitures'),
        path('<int:pk>/', views.FurnitureView.as_view(), name='get_furniture'),
        path('add/', views.AddFurnitureView.as_view(), name='add_furniture'),
        path('add/<int:room>/', views.AddFurnitureView.as_view(), name='add_furniture_to_room'),
        path('<int:pk>/update/', views.UpdateFurnitureView.as_view(), name='update_furniture'),
        path('<int:pk>/delete/', views.DeleteFurnitureView.as_view(), name='delete_furniture'),
    ])),
    path('users/', include([
        path('', views.UsersView.as_view(), name='get_users'),
        path('<int:pk>/', views.UserView.as_view(), name='get_user'),
        path('add/', views.AddUserView.as_view(), name='add_user'),
        path('<int:pk>/update/', views.UpdateUserView.as_view(), name='update_user'),
        path('<int:pk>/delete/', views.DeleteUserView.as_view(), name='delete_user'),
        path('<int:pk>/password/', views.update_user_password, name='password_user'),
    ])),
    path('printer/', include([
        path('room/<int:pk>/', views.RoomPrintView.as_view(), name='print_room'),
        path('box/<int:pk>/', views.BoxPrintView.as_view(), name='print_box'),
        path('furniture/<int:pk>/', views.FurniturePrintView.as_view(), name='print_furniture'),
    ])),
]
