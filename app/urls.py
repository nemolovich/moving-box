"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.generic import RedirectView

from movingbox.admin import admin_site
from qr_code import urls as qr_code_urls

urlpatterns = [
    path('movingbox/', include('movingbox.urls')),
    path('movingbox/admin/', admin_site.urls, name='app_admin'),
    path('movingbox/qr_code/', include(qr_code_urls, namespace="qr_code")),
    path('admin/', admin.site.urls),
    re_path(r'^favicon\.ico$', RedirectView.as_view(url='movingbox/static/favicon.ico')),
]
